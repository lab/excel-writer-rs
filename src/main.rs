fn run_rust_xlsxwriter() {
    let mut workbook = rust_xlsxwriter::Workbook::new();

    let worksheet = workbook.add_worksheet();

    for row in 1..10000 {
        dbg!(row);
        for col in 1..50 {
            worksheet.write(row, col, "1234567890").unwrap();
        }
    }

    workbook.save("out.xlsx").unwrap();
}

fn run_xlsxwriter() {
    let workbook = xlsxwriter::Workbook::new_with_options("out.xlsx", true, None, true).unwrap();

    let mut sheet1 = workbook.add_worksheet(None).unwrap();

    for row in 1..10000 {
        dbg!(row);
        for col in 1..50 {
            sheet1.write_string(row, col, "1234567890", None).unwrap();
        }
    }

    workbook.close().unwrap();


}

fn main(){
    run_xlsxwriter();


}
